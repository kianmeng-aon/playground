import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonComponent } from './common/common.component';

@NgModule({
  imports: [CommonModule],
  declarations: [CommonComponent],
})
export class CommonModule {}
