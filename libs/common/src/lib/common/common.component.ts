import { Component } from '@angular/core';

@Component({
  selector: 'playground-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.scss'],
})
export class CommonComponent {}
